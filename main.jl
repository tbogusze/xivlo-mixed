output = ""
path = ""
file_name = ""
blocks_path = ""
news_path = ""
output_path = ""

for x in readlines("config.yml")
	# Reads values from config.yml
	line = split(x, r": | # ")
	
	if length(line) <= 1
		continue
	end
	
	if line[1] == "output"
		global output_path = line[2]
	elseif line[1] == "path"
		global path = line[2]
	elseif line[1] == "filename"
		global file_name = line[2]
	elseif line[1] == "blocks"
		global blocks_path = line[2]
	elseif line[1] == "news"
		global news_path = line[2]
	else
		println("Warning: " * line[1] * " not found!")
	end
end

blocks = Dict()
for x in readlines(blocks_path)
	temp = split(x, " => ")
	blocks[temp[1]] = temp[2]
end

news = []
for x in readlines(news_path)
	push!(news, x)
end

file = read(path * file_name, String) # reads the file into String

function findTags(text)
	# Finds all tags starting points in a given text.      
	return findall("{% ", text)
end


function saveTags(text)
	# Returns whole tags locations and their content in a given text (both as UnitRange)
	
	names = [] # actual tag content
	whole_tags = [] # whole tags (with "{% ", "endblock" etc.)

	for tag in findTags(text)
		name = UnitRange(tag[end]+1, findnext(" %}", text, tag[end]+1)[1]-1) # fetches the actual tag's content/name
		
		# tries to find endblock section if the tag is a block
		whole_tag = UnitRange(name[1]-3, name[end]+3)

		push!(names, name)
		push!(whole_tags, whole_tag)
	end

	return names, whole_tags
end

function replaceTags(names, whole_tags, text, increase_by)
	# Processes and generates the actual output text
	
	for x in 1:length(whole_tags)
		ind = length(whole_tags)-x+1 # index
		loc = whole_tags[ind] # location
		curr_tag = text[names[ind]] # current tag
		curr_tag_arr = split(curr_tag, " ") # array of words that the tag contains
		println("Replacing tag: " * curr_tag)
		
		part1 = text[1:loc[begin]-1] # part of the text before the tag
		part2 = text[loc[end]+1:end] # part of the text after the tag

		if "block" == curr_tag_arr[1]
			text = part1 * blocks[curr_tag_arr[2]] * part2
		end
		if "include" == curr_tag_arr[1]
			text = part1 * read(path * curr_tag_arr[2][begin+1:end-1], String) * part2
		end
		if "news" == curr_tag_arr[1]
			# TODO: Generate from last to first instead of first to last
			number = parse(UInt8, curr_tag_arr[2]) + increase_by
			if number > 0 && number <= length(news)
				news_part = news[number]
			else
				news_part = "All news have already been generated"
			end
			text = part1 * news_part * part2
		end
	end
	return text
end

function generateFiles()
	# Generates output files
	# TODO: replace tags other than "news" only once
	
	names, whole_tags = saveTags(file)
	news_count = Int8(0)
	
	for x in names
		if (split(file[x], " ")[1] == "news")
			news_count+=1
		end
	end
	
	page_count = Int8(ceil(length(news)/news_count)) # number of pages to generate

	for x in 1:page_count
		println("Generating news page: $x/$page_count")
		open(split(output_path, ".html")[1] * string(x) * ".html", "w") do io #TODO: unhardcode file extension
			write(io, replaceTags(names, whole_tags, file, (x-1)*news_count))
		end
		println()
	end
end

generateFiles()
